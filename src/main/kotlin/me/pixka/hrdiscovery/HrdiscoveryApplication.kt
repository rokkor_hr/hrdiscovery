package me.pixka.hrdiscovery

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer

@SpringBootApplication
@EnableEurekaServer
class HrdiscoveryApplication

fun main(args: Array<String>) {
	runApplication<HrdiscoveryApplication>(*args)
}
